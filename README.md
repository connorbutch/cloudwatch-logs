# internal-cloudwatch-logs

## To run the load tests
./gradlew gatlingRun

## To find all logs at a certain log level using cloudwatch logs search
{ $.level = "INFO" }

## To find all logs at a certain level using cloudwatch logs insights
fields @timestamp, @message
| sort @timestamp desc
| filter level = "INFO"
| limit 20

## To find all logs for a given correlation id (using cloudwatch logs search)
{ $.correlationId = "46e517ca-0e60-4d7b-a10c-b28ef61119ce" }

## To find using cloudwatch logs insights for given correlation id
fields @timestamp, @message
| sort @timestamp desc
| filter correlationId = "88f38a99-0d61-44dc-b6e1-51d11a0deb73"
| limit 20









fields @timestamp, strlen(@message) as errorMessageLength
| sort errorMessageLength asc
| filter level = "ERROR"
| limit 20