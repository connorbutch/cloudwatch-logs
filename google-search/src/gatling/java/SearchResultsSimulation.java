import io.gatling.javaapi.core.Simulation;

import java.time.Duration;
import java.util.*;

import io.gatling.javaapi.core.CoreDsl;
import io.gatling.javaapi.core.ScenarioBuilder;
import io.gatling.javaapi.http.HttpDsl;
import io.gatling.javaapi.http.HttpProtocolBuilder;

import static io.gatling.javaapi.core.CoreDsl.constantUsersPerSec;
import static io.gatling.javaapi.http.HttpDsl.header;
import static io.gatling.javaapi.http.HttpDsl.http;
import static io.gatling.javaapi.http.HttpDsl.status;

public class SearchResultsSimulation extends Simulation {
    private static final String USERS_PER_SECOND_ENV_KEY = "USERS_PER_SECOND";
    private static final String DURATION_IN_SECONDS_ENV_KEY = "DURATION_IN_SECONDS";

    private static final int MIN_USERS_PER_SECOND_ALLOWED = 20;
    private static final int MAX_USERS_PER_SECOND_ALLOWED = 500;
    private static final int DEFAULT_USERS_PER_SECOND = 50;

    private static final int MIN_DURATION_ALLOWED = 2;
    private static final int MAX_DURATION_ALLOWED = 60;
    private static final int DEFAULT_DURATION_IN_SECONDS = 15;

    private static final int MIN_NUMBER_PADDING_SPACES = 15;
    private static final int MAX_NUMBER_PADDING_SPACES = 100;

    private static final Random RANDOM = new Random();

    private static final List<String> QUERIES = List.of(
            "what time is it in New York right now?"
            , "is the moon really made of cheese?"
            , "why are golden retrievers called golden retrievers?"
            , "what is the difference between identical and fraternal twins?"
            , "is supercalifragilisticexpialidocious the longest word in the english language?"
    );

    private final int numberOfUsersPerSecond = getNumberOfUsersPerSecond();
    private final int durationInSeconds = getDurationInSeconds();

    HttpProtocolBuilder httpProtocol = HttpDsl.http
            .baseUrl(System.getProperty("BASE_URL"));


    ScenarioBuilder scn = CoreDsl.scenario("Load test searching for results")
            .feed(() -> {
                List<Map<String, Object>> list = new ArrayList<>();
                for(int i = 0; i < numberOfUsersPerSecond * durationInSeconds; ++i){
                 list.add( Map.of("searchQuery", getSearchQuery()));
                }
                return list.iterator();
            })
            .exec(http("create-customer-request")
                    .get("") //use root of url -- this is required and not a mistake
                    .queryParam("searchQuery", "${searchQuery}")
                    .check(status().is(200))
                    .check(header("x-correlation-id").exists())
                    .check(header("x-request-id").exists())
            );

    public SearchResultsSimulation() {
        setUp(scn.injectOpen(constantUsersPerSec(numberOfUsersPerSecond).during(Duration.ofSeconds(durationInSeconds))))
                .protocols(httpProtocol);
    }

    private int getNumberOfUsersPerSecond() {
        return Optional.ofNullable(System.getenv(USERS_PER_SECOND_ENV_KEY))
                .map(this::parseIntegerFormatSafe)
                .filter(numberOfUsers -> numberOfUsers > MIN_USERS_PER_SECOND_ALLOWED && numberOfUsers < MAX_USERS_PER_SECOND_ALLOWED)
                .orElse(DEFAULT_USERS_PER_SECOND);
    }

    private int getDurationInSeconds() {
        return Optional.ofNullable(System.getenv(DURATION_IN_SECONDS_ENV_KEY))
                .map(this::parseIntegerFormatSafe)
                .filter(durationInSeconds -> durationInSeconds > MIN_DURATION_ALLOWED && durationInSeconds < MAX_DURATION_ALLOWED)
                .orElse(DEFAULT_DURATION_IN_SECONDS);
    }

    private Integer parseIntegerFormatSafe(String intAsString) {
        try {
            return Integer.parseInt(intAsString);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private int getRandomIntegerInRange() {
        return getRandomIntegerInRange(MIN_NUMBER_PADDING_SPACES, MAX_NUMBER_PADDING_SPACES);
    }

    private int getRandomIntegerInRange(int min, int maxInclusive) {
        return min + (int)(Math.random() * ((maxInclusive - min) + 1));
    }

    private String getSearchQuery() {
        int index = getRandomIntegerInRange(0, QUERIES.size()-1);
        String originalQuery = QUERIES.get(index);
        if (originalQuery.length() > 50) {
            int numberOfSpacesToAddForVariety = getRandomIntegerInRange();
            StringBuilder paddingSpacesForVariety = new StringBuilder(originalQuery);
            while (paddingSpacesForVariety.length() < originalQuery.length() + numberOfSpacesToAddForVariety) {
                paddingSpacesForVariety.append("?");
            }
            originalQuery = paddingSpacesForVariety.toString();
        }
        return originalQuery;
    }
}