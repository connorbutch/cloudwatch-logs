package com.connor.structuredlogging;

import java.util.*;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.connor.structuredlogging.Endpoint.Method;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.connor.GitInformation;

public class RequestHandlerGoogleSearchImpl extends AbstractBaseRequestHandler {
    private static final int OK_HTTP_STATUS_CODE = 200;
    private static final int INTERNAL_SERVER_ERROR_STATUS_CODE = 500;

    private static final Logger LOGGER = LogManager.getLogger();

    public RequestHandlerGoogleSearchImpl() {
        super(GitInformation.GIT_COMMIT_HASH);
    }

    @Endpoint(path = "/", method = Method.GET)
    public List<String> performSearch(@QueryParam(parameterKey = "searchQuery") String searchQuery) {
        LOGGER.info("search query: {}", searchQuery);
        if (doThrowError(searchQuery)) {
            String errorMessage = String.format("Oh no, long queries break our logic.  Query ran was %s", searchQuery);
            throw new RuntimeException(String.format(errorMessage));
        }
        return Collections.emptyList();
    }

    @Override
    protected Map<String, String> getAdditionalLogProperties(APIGatewayProxyRequestEvent input) {
        //i'd actually have this take in the args after endpoint had been deduced, but shown here for simplicity
        String queryString = Optional.of(input)
                .map(APIGatewayProxyRequestEvent::getQueryStringParameters)
                .map(queryParamMap -> queryParamMap.get("searchQuery"))
                .orElse("null");
        return Map.of("searchQuery", queryString);
    }

    private boolean doThrowError(String queryString) {
        return queryString != null && queryString.length() > 50; //let's pretend we have a bug in our application that only occurs when users enter long queries
    }
}
