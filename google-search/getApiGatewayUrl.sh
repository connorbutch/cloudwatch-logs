#/bin/sh
#gets the url of the deployed stack -- it does this by reading in the region and stack name from
file="./samconfig.toml"

if [ -f "$file" ]
then
  while IFS='=' read -r key value
  do
    key=$(echo $key | tr '.' '_')

    eval ${key}=\${value} 2>/dev/null || :
  done < "$file"
  regionWithoutQuotes=$(eval echo $region)
  stackNameWithoutQuotes=$(eval echo $stack_name)
  aws cloudformation describe-stacks --stack-name ${stackNameWithoutQuotes} --region ${regionWithoutQuotes} --query "Stacks[0].Outputs[?OutputKey=='GoogleSearchApiUrl'].OutputValue" --output text
else
  echo "$file not found."
fi
