package com.connor.structuredlogging;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;


//NOTE: this only exists to show you that you should have some sort of middleware, real middleware can be
//more complex and I want the focus of this tutorial to be on structured logging
//if there is a desire, I can create another tutorial focusing on middleware like this
//or you can use aws lambda powertools, which does okay (does not include tons of features, but you can adapt it)


public class AbstractBaseRequestHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String CORRELATION_ID_HEADER_KEY = "x-correlation-id";
    private static final String REQUEST_ID_HEADER_KEY = "x-request-id";

    private static final Logger LOGGER = LogManager.getLogger();
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private final String gitCommitHash;

    private APIGatewayProxyRequestEvent input;

    private Context context;
    private String correlationId;
    private String requestId;

    protected AbstractBaseRequestHandler(final String gitCommitHash) {
        this.gitCommitHash = gitCommitHash;
    }

    @Override
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent input, final Context context) {
        correlationId = parseOrGenerateCorrelationId(input);
        ThreadContext.put("correlationId", correlationId);
        requestId = UUID.randomUUID().toString();
        ThreadContext.put("requestId", requestId);
        ThreadContext.put("commitHash", getCommitHash());
        ThreadContext.putAll(getAdditionalLogProperties(input));

        Method methodToInvoke = findMethodToInvoke(input).orElseThrow(() -> new RuntimeException("Oh no, return a 404 to the client here"));
        Object[] methodArgs =  getMethodArgs(methodToInvoke, input);

        APIGatewayProxyResponseEvent response = invokeMethodAndHandleErrors(methodToInvoke, methodArgs);
        Map<String, String> getHeadersIncludingIds = getHeadersIncludingIds(response.getHeaders());
        response.withHeaders(getHeadersIncludingIds);
        LOGGER.info("Returning response {}", response);
        return response;
    }

    private String parseOrGenerateCorrelationId(final APIGatewayProxyRequestEvent input) {
        return Optional.ofNullable(input)
                .map(APIGatewayProxyRequestEvent::getMultiValueHeaders)
                .map(headers -> headers.get(CORRELATION_ID_HEADER_KEY))
                .filter(list -> !list.isEmpty())
                .map(list -> list.get(0))
                .filter(correlationIdStr -> !correlationIdStr.isBlank())
                .orElse(UUID.randomUUID().toString());
    }

    protected Map<String, String> getAdditionalLogProperties(APIGatewayProxyRequestEvent input) {
        return Collections.emptyMap();
    }

    protected String getCorrelationId() {
        return correlationId;
    }

    protected String getRequestId() {
        return requestId;
    }

    protected APIGatewayProxyRequestEvent getInput(){
        return input;
    }

    protected Context getContext(){
        return context;
    }

    protected String getCommitHash(){
        return gitCommitHash;
    }

    private Optional<Method> findMethodToInvoke(final APIGatewayProxyRequestEvent input) {
        //you would really use some reflection here to find the right method to invoke based on annotations so it is dynamic
        //then we would parse the parameters and their annotations to populate the parameters from the input
        //it's not slow if you only search this class
        return Arrays.stream(getClass().getMethods()).filter(method -> "performSearch".equals(method.getName())).findFirst();
    }

    private Object[] getMethodArgs(Method method, APIGatewayProxyRequestEvent input){
        //you would parse the parameters and their annotations to populate the parameters from the input
        return new Object[]{ Optional.of(input)
                .map(APIGatewayProxyRequestEvent::getQueryStringParameters)
                .map(queryParamMap -> queryParamMap.get("searchQuery"))
                .orElse(null) };
    }

    private APIGatewayProxyResponseEvent invokeMethodAndHandleErrors(Method methodToInvoke, Object[] methodArgs){
        String responseBody = "{}";
        int statusCode = 500;
        try {
            try {
                responseBody = OBJECT_MAPPER.writeValueAsString(methodToInvoke.invoke(this, methodArgs));
                statusCode = 200;
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        } catch (IllegalAccessException e) {
            LOGGER.error("Could not access method via reflection from controller", e);
        } catch (InvocationTargetException e) {
            //NOTE: Here you would actually search for methods in class annotated for exception handler to leave each lambda with their own specific error handling
            //this specific log message is just here for the sake of the demo
            LOGGER.error("Error with query: {}", methodArgs[0], e);
        }
        return new APIGatewayProxyResponseEvent().withBody(responseBody).withStatusCode(statusCode);
    }

    private Map<String, String> getHeadersIncludingIds(Map<String, String> existingHeaders){
        Map<String, String> headersWithIds = new HashMap<>();
        if(existingHeaders != null){
            headersWithIds.putAll(existingHeaders);
        }
        headersWithIds.putAll(Map.of(CORRELATION_ID_HEADER_KEY, correlationId, REQUEST_ID_HEADER_KEY, requestId));
        return headersWithIds;
    }
}
