package com.connor.structuredlogging;

public @interface QueryParam {
    String parameterKey();
}
