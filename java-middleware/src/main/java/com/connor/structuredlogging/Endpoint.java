package com.connor.structuredlogging;

public @interface Endpoint {
    String path();
    Method method();

    public static enum Method {
        GET
        , POST
        , PUT
        , PATCH
        , DELETE
        , HEAD
        ;
    }
}
