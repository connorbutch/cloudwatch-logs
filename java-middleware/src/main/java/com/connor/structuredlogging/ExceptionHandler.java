package com.connor.structuredlogging;

public @interface ExceptionHandler {
    Class<? extends Exception> exceptionClass();
    int responseStatusCode();
}
